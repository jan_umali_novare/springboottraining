package hk.com.novare.demo.service;

import hk.com.novare.demo.entity.UserEntity;

public interface UserService {
    void sendEmail(UserEntity ue);
}
