package hk.com.novare.demo.service;

import hk.com.novare.demo.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    private MailSender ms;

    @Autowired
    public void setMailSender(MailSender ms){
        this.ms = ms;
    }

    public void sendEmail(UserEntity ue){

        System.out.println("test");
        SimpleMailMessage message = new SimpleMailMessage();

        message.setTo(ue.getEmail());
        message.setSubject(ue.getMailSubj());
        message.setText(ue.getMailBody());

        ms.send(message);

    }
}
