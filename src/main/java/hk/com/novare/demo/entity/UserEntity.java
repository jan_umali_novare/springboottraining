package hk.com.novare.demo.entity;

public class UserEntity {

    private String name;
    private String email;
    private String mailSubj;
    private String mailBody;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMailSubj() {
        return mailSubj;
    }

    public void setMailSubj(String mailSubj) {
        this.mailSubj = mailSubj;
    }

    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }
}
