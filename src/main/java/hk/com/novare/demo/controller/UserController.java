package hk.com.novare.demo.controller;


import hk.com.novare.demo.entity.UserEntity;
import hk.com.novare.demo.service.UserService;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {


    private UserService us;

    @Autowired
    public UserController(UserService us){
        this.us = us;
    }

    @PostMapping("/save")
    public UserEntity emailSender(@RequestBody UserEntity ue){
        us.sendEmail(ue);
        return ue;
    }
}
